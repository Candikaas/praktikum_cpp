/*

   Nama Program : tunja1.c

   Tgl buat     : 10 Oktober 2023

   Deskripsi    : menghitung besarnya jumlah tunjangan

*/
#include <iostream>
#include <stdlib.h>
#include <iomanip>

using namespace std;

int main()
{
    system("clear");

    int JumlahAnak = 0;
    float GajiKotor = 0.0, Tunjangan = 0.0, PersenTunjangan = 0.0, gajibersih = 0.0;

    PersenTunjangan = 0.2;
    cout << "Gaji Kotor ? ";
    cin >> GajiKotor;
    cout << "Jumlah Anak ? ";
    cin >> JumlahAnak;
    if (JumlahAnak > 2)
    {
        PersenTunjangan = 0.3;
    }
    Tunjangan = PersenTunjangan * GajiKotor;
    gajibersih = GajiKotor - Potongan + Tunjangan;
    cout << "Besar Tunjangan = Rp " << setprecision(2) << Tunjangan << endl;
    cout << "Gaji bersih = Rp " << setprecision(2) << gajibersih << endl;

    return 0;
}

